﻿using System;

namespace pb2
{
    class Program
    {

        static int[] moveZerosToTheBack( int[] arr, int n)
        { 
            int count = 0;

       
            for (int i = 0; i < n; i++)
                if (arr[i] != 0)

                    arr[count++] = arr[i];

            while (count < n)
                arr[count++] = 0;

            return arr;
        }
        public static int[] extractNumbersFromString()
        {
            string[] lines= System.IO.File.ReadAllLines(@"C:\Users\AL073094\source\repos\Tema 4\pb2\data1.txt");
            String oneLineFile = String.Join(" ", lines);
            int[] result = new int[0];
            int number = 0;
            int k = 0;
            for(int i=0;i<oneLineFile.Length;i++)
            {
                if(Char.IsNumber(oneLineFile[i]))
                {
                    number = number * 10 + (int)Char.GetNumericValue(oneLineFile[i]);
                }
                else
                {
                    Array.Resize(ref result, result.Length + 1);
                    result[k++] = number;
                    number = 0;
                }
            }
            return result;

        }

        public static void Main()
        {

            int[] array = extractNumbersFromString();
            array=moveZerosToTheBack( array, array.Length);
            Console.WriteLine("Array after moving back the all the zeros ");
            for (int i = 0; i < array.Length; i++)
                Console.Write(array[i] + " ");
        }
    }
}
