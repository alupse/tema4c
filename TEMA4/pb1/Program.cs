﻿using System;

namespace Tema_4
{
    class Program
    {
        public static int[] concatenateArrays(int[] firstArray, int[] secondArray,
                                   int sizeOfFirstArray, int sizeOfSecondArray)
        {
            int i = 0,
                j = 0,
                k = 0;
            int[] thirdArray = new int[sizeOfFirstArray+sizeOfSecondArray];



            while (i < sizeOfFirstArray && j < sizeOfSecondArray)
            {
                if (firstArray[i] < secondArray[j])
                    thirdArray[k++] = firstArray[i++];
                else
                    thirdArray[k++] = secondArray[j++];
            }

            while (i < sizeOfFirstArray)
                thirdArray[k++] = firstArray[i++];

            while (j < sizeOfSecondArray)
                thirdArray[k++] = secondArray[j++];

            return thirdArray;
        }

        public static (int[] firstArray, int[] secondArray) ExtractTheNumbersFromFile()
        {

            int[] firstArray = new int[0] ;
            int[] secondArray = new int[0];
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\AL073094\source\repos\Tema 4\Tema 4\data.txt");
            String oneLineFile = String.Join("!", lines);
            bool onFirstLine = true;
            int k=0;
            int number=0;
            for(int i=0;i<oneLineFile.Length;i++)
            {
                if(Char.IsNumber(oneLineFile[i]))
                {
                    number=(number*10)+(int) Char.GetNumericValue(oneLineFile[i]);
                }
                else if(oneLineFile[i]==',' && onFirstLine==true)
                {
                    Array.Resize(ref firstArray, firstArray.Length+1);
                    firstArray[k++] = number;
                    number = 0;
                }
                if(oneLineFile[i] == ',' && onFirstLine == false)
                {
                    
                    Array.Resize(ref secondArray, secondArray.Length+1);
                    secondArray[k++] = number;
                    number = 0;
                }
                
                if(oneLineFile[i] == '!')
                {
                   
                    Array.Resize(ref firstArray, firstArray.Length + 1);
                    firstArray[k++] = number;
                    number = 0;
                    onFirstLine = false;
                    k = 0;
                }
            }
            return (firstArray, secondArray);
        }

        // Driver code 
        public static void Main()

        {
            //int[] firstArray = Array.Empty<int>();
            //int[] secondArray= Array.Empty<int>();
           
            (int[] firstArray, int[] secondArray) = ExtractTheNumbersFromFile();
            int[] resultArray=concatenateArrays(firstArray, secondArray, firstArray.Length, secondArray.Length);
           

            Console.Write("Array after merging\n");
              for (int i = 0; i < firstArray.Length+secondArray.Length; i++)
                 Console.Write(resultArray[i] + " ");
        }
    }

    // This code is contributed 
    // by ChitraNayal 
}

