﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace pb3
{
    class Program
    {
        public static int NumberOfOccurences(string inputString, Stream stream)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                throw new ArgumentNullException(nameof(inputString));
            }

            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (!stream.CanRead)
            {
                throw new ArgumentException("Stream cannot be read");
            }

            StreamReader reader = new StreamReader(stream);
            Regex r = new Regex(inputString);
            MatchCollection matches = r.Matches(reader.ReadToEnd());
            return matches.Count;


        }

        static void Main(string[] args)
        {
            var inputTest = "abc";
            // can use any kind of stream: MemoryStream, StreamReader etc.
            var stream = new MemoryStream(Encoding.ASCII.GetBytes("abcTeST\n this is abc a test\n yes"));
            Console.WriteLine(NumberOfOccurences(inputTest, stream)); // se va afisa 2

        }
    }
}
